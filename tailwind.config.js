/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./public/**/*.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
        "./node_modules/flowbite/**/*.{js,vue}",
    ],
    theme: {
        extend: {},
    },
    plugins: [require("flowbite/plugin")],
};
