import { createRouter, createWebHashHistory } from "vue-router";
import PrivacyPolicy from "../components/PrivacyPolicy.vue";
import TermOfUse from "../components/TermOfUse.vue";
import HomeView from "../views/HomeView.vue";
const routes = [
    {
        path: "/",
        name: "/Home",
        component: HomeView,
    },
    {
        path: "/Privacy-policy",
        name: "PrivacyPolicy",
        component: PrivacyPolicy,
    },
    {
        path: "/Term-of-use",
        name: "TermOfUse",
        component: TermOfUse,
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
